package pl.edu.pwsztar

import spock.lang.Specification
import spock.lang.Unroll

class CreateAccountSpec extends Specification {

    @Unroll
    def "should create account number #accountNumber for #user"() {

        given: "initial data"
            def bank = new Bank(accService)
        when: "the account is created"
            def number = bank.createAccount()
        then: "check account number"
            number == accountNumber

        where:
            user   | accountNumber
            'John' | 1
            'Tom'  | 2
            'Mike' | 3
            'Todd' | 4
    }
    def "create new account with balance = 0" (){
        given: "initial data"
            def bank = new Bank()
        when: "creating account"
            int accountNumber = bank.createAccount()
        then: "account balance = 0"
            Account newAccount = bank.getAccount(accountNumber)
        newAccount.getAccountBalance() == 0
    }
}
