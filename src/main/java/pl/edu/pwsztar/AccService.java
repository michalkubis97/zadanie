package pl.edu.pwsztar;
import java.util.List;
public interface AccService {
    Account getAccountByNumber(int accountNumber);
    Account saveAccount(Account account);
    void deleteAccount(int accountNumber);
    List<Account> findAllAccounts();
}
