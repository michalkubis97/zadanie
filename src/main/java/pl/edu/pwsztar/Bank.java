package pl.edu.pwsztar;

// TODO: Prosze dokonczyc implementacje oraz testy jednostkowe
// TODO: Prosze nie zmieniac nazw metod - wszystkie inne chwyty dozwolone
// TODO: (prosze jedynie trzymac sie dokumentacji zawartej w interfejsie BankOperation)

import java.util.Optional;

class Bank implements BankOperation {

    private static int accountNumber = 0;
    private final AccService accService;
    private static final int EMPTY_ACCOUNT = 0;
    Bank(AccService accService) {
        this.accService = new AccStorage();
    }

    public int createAccount() {
        ++accountNumber;
        accService.saveAccount(new Account(accountNumber, EMPTY_ACCOUNT));
        return accountNumber;
    }

    public int deleteAccount(int accountNumber) {
        final int saldo = accountBalance(accountNumber);
        if(saldo == ACCOUNT_NOT_EXISTS){
            System.out.println("Acc with num + "+accountNumber+" not exists");
        }else{
            accService.deleteAccount(accountNumber);
        }
        return saldo;
    }
    public Account getAccount(int accountNumber){
        return accService.getAccountByNumber(accountNumber);
    }

    public boolean deposit(int accountNumber, int amount) {
        final Account account = getAccount(accountNumber);
        if(Optional.ofNullable(account).isPresent()){
            account.accountDeposit(amount);
            accService.saveAccount(account);
        }
        return false;
    }

    public boolean withdraw(int accountNumber, int amount) {
        return false;
    }

    public boolean transfer(int fromAccount, int toAccount, int amount) {

        return false;
    }

    public int accountBalance(int accountNumber) {
        final Optional<Account> account = Optional.ofNullable(getAccount(accountNumber));
        return account.map(Account::getAccountBalance).orElse(ACCOUNT_NOT_EXISTS);
    }

    public int sumAccountsBalance() {

        return 0;
    }
}
